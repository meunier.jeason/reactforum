import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";

import shortid from 'shortid';
import { API } from './components/constants.js';
import Cookie from 'js-cookie';
import jwt_decode from 'jwt-decode';



import LoadingSpinner from './components/shared/utils/loadingSpinner'

import Background from './components/background';
import NavigationBar from './components/shared/navbar';
import Home from './components/home';
import Search from './components/search';
import Category from './components/category';
import Post from './components/post';
import NewPost from './components/newPost';
import User from './components/user';
import NoPage from './components/noPage';
import { Fragment } from 'react';


class App extends Component {
  
  /**
   * Creates an instance of App.
   * @param {*} props
   * @memberof App
   */
  constructor(props) {
    super(props);
    this.state = {
      tokenPayload: {},
      isAuth: false,
      loading: true,
      userInfos : {
        id: '',
        email: '',
        username: '',
        roles: []
      },
      categoriesReload : false,
    };
    // bind des fonctions
    this.postLogin = this.postLogin.bind(this);
    this.setUserInfos = this.setUserInfos.bind(this);
    this.logOut = this.logOut.bind(this);
    this.reloadCategories = this.reloadCategories.bind(this)
  }
  
  // promesse pour le loading spinner au chargement de la page
  /**
   * Fonction permettant un affichage du loader avec durée choisi
   *
   * @return {*} 
   * @memberof App
   */
  loadingTime(){
    return new Promise(resolve => setTimeout(resolve, 1500))
  }

  /**
   * Lance le Loading Spinner et récupere le token dans le cookie jwt_hp
   * Si le token est retourné (signifie que le cookie existe), on lance setTokenInfo
   *
   * @memberof App
   */
  componentDidMount() {
    this.loadingTime().then(() => {
      //recherche du cookie jwt_hp
      let jwtHP = Cookie.get().jwt_hp;
      //si il existe, alors je lance la fonction setTokenInfo 
      jwtHP ? this.setTokenInfo(jwtHP) : this.setState({loading:false});
    })
  }


  
  /**
   * appeler par componentDidMount si le token jwt_hp existe. Insere les données du
   * token (payload) dans un state
   * Appel à setUserInfos()
   *
   * @param {JWT TOKEN} jwtHP
   * @memberof App
   */

  async setTokenInfo(jwtHP){
    let payload =  await jwt_decode(jwtHP, { payload: true });

    let tokenPayload = {username: payload.username, iat: payload.iat, exp: payload.exp};
    this.setState(state => ({
      tokenPayload,
      isAuth : true      
    }));
    this.setUserInfos(payload.username);

  }  

  /**
   * Appeler par setTokenInfo(), Récupere les infos de l'utilisateur
   * avec un appel à l'api grace au "username" récupéré dans le cookie
   *
   * @param {string} username
   * @memberof App
   */
  setUserInfos(username) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include'
    };
    fetch(`${API}/users?username[]=`+username, requestOptions)
    .then(res => res.json())
      .then(res => {
        let userInfos = {
          id: res['hydra:member'][0].id,
          email: res['hydra:member'][0].email,
          username: res['hydra:member'][0].username,
          roles: res['hydra:member'][0].roles
        };
        this.setState({
          userInfos: userInfos,
          loading: false
        });
      })
  }


  /**
   * Fonction de connexion, envoi une requete à l'api
   * L'api retourne deux cookies contenant le token découpé
   * un cookie contient le Header et le Payload
   * l'autre contient la signature
   *
   * @param {string} username
   * @param {string} password
   * @return {boolean} 
   * @memberof App
   */
  postLogin(username, password) {
    if(!this.state.isAuth){
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include',
        body: JSON.stringify({ username: username, password:password })
      };
      let sendRequest = async() =>{ 
        try{
          const res = await fetch(`${API}/login`, requestOptions);
          console.log(res)
          let jwtHP = Cookie.get().jwt_hp;
          jwtHP && this.setTokenInfo(jwtHP);
          return true
        } catch(err) {
          console.error('err')
          return false
        }
      }
      let status = sendRequest()

      return status  
    } else {
      console.log('Deja connecté')
    }

  }

  /**
   * Fonction de déconnexion, detruit le cookie jwt_hp
   * Le token n'est plus complet et ne peut plus etre
   * envoyé à l'api. Reset des states contenant les infos du user
   *
   * @memberof App
   */
  logOut(){
    if(Cookie.get().jwt_hp){
      Cookie.remove('jwt_hp', { path: '/', domain: 'forum.boom-ker.live' })
      Cookie.remove('jwt_s', { path: '/', domain: 'forum.boom-ker.live' })

      this.setState({
        tokenPayload: {},
        userInfos : {
          id: '',
          email: '',
          username: '',
          roles: []
        },
        isAuth: false
      });
    } else console.log('PAS CONNECTE')
  }

  /**
   * Fonction qui va lancer un render de la navbar
   * Utile lorsque un admin ajoute/supprime une catégorie
   *
   * @memberof App
   */
  reloadCategories(){
    this.setState({
      categoriesReload : shortid.generate()
    })
  }

  render() {
    return (
      <Fragment>
        {/* Si le state loading est vrai, affiche le spinner sinon met en place le routeur */}
        {this.state.loading ? 
          <LoadingSpinner/>
          : 
          <Router>
            <Background />
            <NavigationBar login={this.postLogin} logout={this.logOut} username={this.state.tokenPayload.username} id={this.state.userInfos.id} key={this.state.categoriesReload} loading={this.state.loading}/>
            <Switch>
              <Route exact path="/" render={(props) => <Home key={shortid.generate()} userInfos={this.state.userInfos} isAuth={this.state.isAuth} {...props} />} />
              <Route exact path="/search/:criteria" render={(props) => <Search key={shortid.generate()} userInfos={this.state.userInfos} isAuth={this.state.isAuth} {...props} />}/>
              <Route exact path="/createpost" render={(props) => <NewPost key={shortid.generate()} isAuth={this.state.isAuth} {...props} />}/>
              <Route exact path="/category/:id" render={(props) => <Category key={shortid.generate()} userInfos={this.state.userInfos} isAuth={this.state.isAuth} {...props} />} />
              <Route exact path="/post/:id/:slug" render={(props) => <Post key={shortid.generate()} userInfos={this.state.userInfos}  isAuth={this.state.isAuth} {...props} />} />
              <Route exact path="/user/:id/:username" render={(props) => <User key={shortid.generate()} userInfos={this.state.userInfos} isAuth={this.state.isAuth} reload={this.reloadCategories} {...props} />} />
              <Route render={(props) => <NoPage key={shortid.generate()} {...props} />} />
            </Switch>

          </Router>
        }
      </Fragment>
    );
  }
}
export default App;