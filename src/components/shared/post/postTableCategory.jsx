import React, { Component } from 'react';
import { API } from '../../constants.js';
import TableLine from './TableLine';
import Pagination from '../utils/pagination'

/**
 * Composant affichant la grille des articles d'une catégorie
 * Enfant de Category, Parent de TableLine et de Pagination
 * Props requis: id de la catégorie souhaitée, infos user connecté, isAuth(boolean de connexion)
 *
 * @class PostTableCategory
 * @extends {Component}
 */
class PostTableCategory extends Component {
    
    state = {
        posts: []
    }

    /**
     * Appel à l'api pour récuperer les posts de la catégorie
     *
     * @param {number} [newPage=1] Page à charger
     * @memberof PostTableCategory
     */
    getPosts(newPage = 1){
        const page = newPage;
        const category = this.props.id;
        const filters = `?category=${category}&order[createdAt]=desc&page=${page}`;
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/posts`+filters, requestOptions)
        .then(res => res.json())
          .then(res => {
                let nbPages;
                if(res['hydra:view'].hasOwnProperty(['hydra:last'])){
                const iriLastPages = res['hydra:view']['hydra:last'];
                const chars = iriLastPages.split('&page=');
                nbPages = chars[1]
                } else{
                    if(this.state.currentPage !== 1){
                        this.getPosts(1)
                    }else{
                        nbPages = "1";
                    }
                }

            this.setState({ 
                posts: res['hydra:member'],
                nbPages: nbPages,
                currentPage: page
            })
            window.scrollTo(0, 0)
          })
    }

    /**
     * Permet le reload des posts en cas de changement (update/delete)
     * d'un post depuis l'application. Prend la page en cours en parametre.
     *
     * @param {number} newPage
     * @memberof PostTableCategory
     */
    pageChange(newPage){
        this.getPosts(newPage);
    }

    /**
     * Lance la fonction qui récupere les infos depuis l'api
     *
     * @memberof PostTableCategory
     */
    componentDidMount(){
        this.getPosts();
    }

    render(){
        return (
            <div className="container mt-1">
                <div className="row">
                    {this.state.posts.map((post,i , arr) => {
                        return(
                            (this.props.userConnect.username === post.author.username || this.props.userConnect.roles.includes("ROLE_ADMIN")) ?
                                <TableLine key={post.id} postInfos={post} updateList={() => this.pageChange(this.state.currentPage)} updateDel={true}/>
                            :
                                <TableLine key={post.id} postInfos={post} updateList={() => this.pageChange(this.state.currentPage)} updateDel={false}/>
                        )                                   
                    })}
                </div>
                {this.state.nbPages > 1 && <Pagination maxPage={this.state.nbPages} currentPage={this.state.currentPage} pageChange={this.pageChange.bind(this)}/>}

            </div>
        );
    }
}
export default PostTableCategory;