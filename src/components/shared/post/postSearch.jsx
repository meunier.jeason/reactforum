import React, { Component, Fragment } from 'react';
import { API } from '../../constants.js';
import PostLine from './postLine';
import Pagination from '../utils/pagination';

import Spinner from 'react-bootstrap/Spinner'
import Table from 'react-bootstrap/Table';

/**
 * Composant affichant les tableaux de recherche de post
 * Enfant de Search, Parent de PostLine et Pagination
 * Props requis: criteria(string a rechercher), userConnect (infos user connecté), isAuth (boolean de connexion)
 *
 * @class PostSearch
 * @extends {Component}
 */
class PostSearch extends Component {
    
    state = {
        postsContent: [],
        postsTitle: [],
        loadTitle : false,
        loadContent: false,
    }

    /**
     * Appel à l'api pour récuperer les posts où le
     * titre contient le critere de recherche
     *
     * @param {number} [newPage=1] Page à charger
     * @memberof PostSearch
     */
    getPostByTitle(newPage = 1){
        const page = newPage;
        const filters = `?order[createdAt]=desc&title=${this.props.criteria}&page=${page}`;
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/posts`+filters, requestOptions)
        .then(res => res.json())
          .then(res => {
            let nbPagesTitle;
            if(res['hydra:view'].hasOwnProperty(['hydra:last'])){
              const iriLastPages = res['hydra:view']['hydra:last'];
              const chars = iriLastPages.split('&page=');
              nbPagesTitle = chars[1]
            } else{
                if(this.state.currentPageTitle !== 1){
                  this.getPostByTitle(1)
                }else{
                  nbPagesTitle = "1";
                }
            }
            this.setState({ 
                postsTitle: res['hydra:member'],
                nbPagesTitle: nbPagesTitle,
                currentPageTitle: page,
                loadTitle: true
            })
        })
    }

    /**
     * Appel à l'api pour récuperer les posts où le
     * contenu contient le critere de recherche
     *
     * @param {number} [newPage=1]
     * @memberof PostSearch
     */
    getPostByContent(newPage = 1){
        const page = newPage;
        const filters = `?order[createdAt]=desc&content=${this.props.criteria}&page=${page}`;
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/posts`+filters, requestOptions)
        .then(res => res.json())
          .then(res => {
            let nbPagesContent;
            if(res['hydra:view'].hasOwnProperty(['hydra:last'])){
              const iriLastPages = res['hydra:view']['hydra:last'];
              const chars = iriLastPages.split('&page=');
              nbPagesContent = chars[1]
            } else{
                if(this.state.currentPageContent !== 1){
                  this.getPostByContent(1)
                }else{
                  nbPagesContent = "1";
                }
            }
            this.setState({ 
                postsContent: res['hydra:member'],
                nbPagesContent: nbPagesContent,
                currentPageContent: page,
                loadContent: true
            })
        })
    }

    /**
     * Lance les fonctions qui récupere les infos depuis l'api
     *
     * @memberof PostSearch
     */
    componentDidMount(){
        this.getPostByContent();
        this.getPostByTitle();

    }

    /**
     * Fonction permettant le reload du tableau 
     * recherchant par contenu
     *
     * @param {number} newPage Page actuelle
     * @memberof PostSearch
     */
    pageChangeContent(newPage){
            this.getPostByContent(newPage); 
    }
    
    /**
     * Fonction permettant le reload du tableau
     * recherchant par titre
     *
     * @param {number} newPage Page actuelle
     * @memberof PostSearch
     */
    pageChangeTitle(newPage){
        this.getPostByTitle(newPage);
    }

    render(){
        console.log(this.props.userConnect)
        return (
            <div className="container mt-2" >
                <h5>Recherche dans le titre des posts:</h5>
                { this.loadContent ?
                    <Fragment>
                        <Spinner animation="border" variant="primary" />
                        <Spinner animation="border" variant="secondary" />
                        <Spinner animation="border" variant="success" />
                        <Spinner animation="border" variant="danger" />
                        <Spinner animation="border" variant="warning" />
                        <Spinner animation="border" variant="info" />
                    </Fragment>
                :
                    this.state.postsTitle.length > 0 ?
                        <Fragment>
                            <Table className="bg-light" striped bordered hover size="sm">
                                <thead>
                                    <tr>
                                        <th>Etat</th>
                                        <th>Titre</th>
                                        <th>Propriétaire</th>
                                        <th className="d-none d-md-table-cell">Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.postsTitle.map((post,i , arr) => {
                                        return(
                                            <PostLine key={post.id} postInfos={post} userConnect={this.props.userConnect} updateList={() => this.pageChangeTitle(this.state.currentPageTitle)} /> 
                                        )                                   
                                    })}
                                </tbody>
                            </Table>
                            {this.state.nbPagesTitle > 1 && <Pagination maxPage={this.state.nbPagesTitle} currentPage={this.state.currentPageTitle} pageChange={this.pageChangeTitle.bind(this)}/>}
                        </Fragment>
                    :
                        <div className="alert alert-warning text-center">
                            <h5>Aucun titre ne correspond à la recherche!</h5>
                        </div>
                }
                <h5>Recherche dans le contenu des posts:</h5>
                {this.loadContent ?
                    <Fragment>
                        <Spinner animation="border" variant="primary" />
                        <Spinner animation="border" variant="secondary" />
                        <Spinner animation="border" variant="success" />
                        <Spinner animation="border" variant="danger" />
                        <Spinner animation="border" variant="warning" />
                        <Spinner animation="border" variant="info" />
                    </Fragment>
                :
                    this.state.postsTitle.length > 0 ?
                        <Fragment>
                            <Table className="bg-light" striped bordered hover size="sm">
                                <thead>
                                    <tr>
                                        <th>Etat</th>
                                        <th>Titre</th>
                                        <th>Propriétaire</th>
                                        <th className="d-none d-md-table-cell">Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.postsContent.map((post,i , arr) => {
                                        return(
                                            <PostLine key={post.id} postInfos={post}  userConnect={this.props.userConnect} updateList={() => this.pageChangeContent(this.state.currentPageContent)} /> 
                                        )                                   
                                    })}
                                </tbody>
                            </Table>
                            {this.state.nbPagesContent > 1 && <Pagination maxPage={this.state.nbPagesContent} currentPage={this.state.currentPageContent} pageChange={this.pageChangeContent.bind(this)}/>}
                        </Fragment>
                    :
                        <div className="alert alert-warning text-center">
                            <h5>Aucun contenu ne correspond à la recherche!</h5>
                        </div>
                }

            </div>
        );
    }
}
export default PostSearch;