import React from 'react';
import Spinner from 'react-bootstrap/Spinner'

/**
 * Composant graphique, permettant l'affichage d'un 
 * Spinner sur les phases de chargement
 *
 * @param {*} props
 * @return {*} 
 */
function LoadingSpinner(props) {

    return(
      <div style={{height:"100vh"}} className="d-flex justify-content-center align-items-center">

        <Spinner animation="grow" variant="primary" />
        <Spinner animation="grow" variant="secondary" />
        <Spinner animation="grow" variant="success" />
        <Spinner animation="grow" variant="danger" />
        <Spinner animation="grow" variant="warning" />
        <Spinner animation="grow" variant="info" />
      </div>
    )
  }

export default LoadingSpinner