import React, { Component } from 'react';


/**
 * Composant affichant la pagination 
 * contenu dans tous les composants ayant une pagination
 * Props requis: maxPage (int nombre de page totale), currentPage (int page actuelle),
 * pageChange (fonction d'update du parent avec les données de la nouvelle page)
 *
 * @class Pagination
 * @extends {Component}
 */
class Pagination extends Component {
  /**
   * Creates an instance of Pagination.
   * @param {maxPage, currentPage, pageChange} props
   * @memberof Pagination
   */
  constructor(props) {
    super(props);
    
    this.state = {
      maxPage: parseInt(props.maxPage,10),
      currentPage: parseInt(props.currentPage,10),
      first: '\u00ab',
      prev: '\u2039',
      next: '\u203A',
      last: '\u00bb',
    };
  }

  /**
   * Fonction qui handle le clic sur un bouton de page
   *
   * @param {event} e
   * @param {int} page Nouvelle page demandé
   * @memberof Pagination
   */
  handleClick = (e, page) => {
    //this.props.pageChange(page);
    if(page !== this.props.currentPage){
      this.props.pageChange(page);
    }
  };

  render(){
    let pageArray = [];
    let minus = parseInt(this.props.currentPage) - 3;
    let max = parseInt(this.props.currentPage) + 3;
    if(this.props.maxPage <= 5){
      for (let page = 1; page <= this.props.maxPage; page++) {
        pageArray.push(page)
      }
    }else if(minus >= 2 && max <= this.props.maxPage-1){
      for (let page = minus; page <= max; page++) {
        pageArray.push(page)
      }
    }
    else if(minus <= 1){
      for (let page = 1; page <= max; page++) {
        pageArray.push(page)
      }
    }
    else if(max >= this.props.maxPage){
      for (let page = minus; page <= this.props.maxPage; page++) {
        pageArray.push(page)
      }
    }
    return(
      
      <div className="mt-1" key={this.props.currentPage}>
        <ul className="pagination d-flex justify-content-center">
          {!(minus <= 1) && <li key="first" value={this.state.first}  onClick={event => this.handleClick(event, 1)}>{this.state.first}</li>}
          {this.props.currentPage > 1 && <li key="prev" value={this.props.currentPage-1} onClick={event => this.handleClick(event, this.props.currentPage-1)} >{this.state.prev}</li>}
          {pageArray.map((page,i , arr) => {
            return(
              this.props.currentPage === page ? 
                <li key ={page} value={page}  onClick={event => this.handleClick(event, page)} className="active">{page}</li>
              :
                <li key={page} value={page} onClick={event => this.handleClick(event, page)} >{page}</li>
            )                                   
          })}
          {this.props.currentPage < this.props.maxPage &&<li key="next" onClick={event => this.handleClick(event,this.props.currentPage+1 )} value={this.props.currentPage+1}>{this.state.next}</li>}
          {!(max >= this.props.maxPage) && <li key="last"onClick={event => this.handleClick(event,this.props.maxPage)} value={this.props.maxPage}>{this.state.last}</li>} 
      </ul>
    </div> 
        
  );
}
    



}

export default Pagination