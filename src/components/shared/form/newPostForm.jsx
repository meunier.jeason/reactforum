import React, {Component, Fragment} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import { Redirect } from 'react-router';
import { API } from '../../constants';
import Tags from './tags';

/**
 * Composant du formulaire d'ajout d'un post
 * Enfant de NewPost, Parent de Tags
 * Props requis: showToast (fonction parente affichant un toast d'erreur), 
 * categories (Object contenant les catégories du forum),
 * tags (Array contenant les tags du forum)
 *
 * @class NewPostForm
 * @extends {Component}
 */
class NewPostForm extends Component {
    
    /**
     * Creates an instance of NewPostForm.
     * @param {showToast, categories, tags} props
     * @memberof NewPostForm
     */
    constructor(props) {
        // const [state, setstate] = useState(false)
        super(props);
        this.state = {
            redirect: false,
            redirectLink: null,
            categories: props.categories,
            tags: props.tags,
            title: null,
            category: null,
            content: null,
            postTags: [],
            errors: {
                postTitle: 'Merci de remplir le titre de votre post',
                postCategory: 'Merci de selectionner une catégorie',
                postContent: 'Merci de remplir le contenu de votre post'
            }
        };
    }

    // Submit du post
    /**
     * Fonction qui handle le submit du formulaire
     * Si formulaire valide, alors envoi la réponse modifié à l'api
     *
     * @param {event} event
     * @memberof NewPostForm
     */
    handleSubmit = (event) => {
        event.preventDefault();
        if(this.validateForm(this.state.errors)) {
            console.info('Valid Form')
            let {title, content, postTags, category} = this.state
            let tagsObject = postTags.map(elem => {
                return({"title":elem})
            })
            let data = {
                title : title,
                content: content,
                tags: tagsObject,
                category: `/api/category/${category}`,
                // state: "/api/state/1"
            }
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                credentials: 'include',
                body: JSON.stringify(data)
            };
            fetch(`${API}/post`, requestOptions)
            .then(res => res.json())
                .then(res => {

                    this.setState({
                        redirect: true,
                        redirectLink: `/post/${res.id}/${res.slug}`
                    })

                })
                .catch(console.log)
        }else{
            console.error('Invalid Form')
            this.props.showToast()
        }
    }
    //Permet de verifier si il ya des erreurs (au cas ou le "disabled" du bouton est supprimé dans la console)
    /**
     * Fonction de validation du formulaire, appelé par handleSubmit()
     * Prend l'objet contenant les erreurs en parametre (venant du state)
     *
     * @param {Object} errors
     * @memberof NewPostForm
     */
    validateForm = (errors) => {
        let valid = true;
        Object.values(errors).forEach(
          // Si les errors ne sont pas vide, passe le valid à false
          (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    //Permet de verifier en temps réel si les champs sont correctements remplis
    /**
     * Fonction qui controle les champs du formulaire
     * Affiche les erreurs en temps réel et les met dans le state
     *
     * @param {event} event
     * @memberof NewPostForm
     */
    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;
      
        switch (name) {
          case 'title': 
            errors.postTitle = 
              value.length < 6
                ? 'Le taille du titre doit etre supérieure à 5 charactères!'
                : '';
            break;
            case 'category': 
            errors.postCategory = 
              value === ''
                ? 'Merci de selectionner une catégorie'
                : '';
            break;
            case 'content': 
            errors.postContent = 
            value.length < 31
                ? 'Le taille du contenu doit etre supérieure à 30 charactères!'
                : '';
            break;
          default:
            break;
        }
      
        this.setState({errors, [name]: value})
    }

    /**
     * Fonction qui set les nouveaux tags du post
     *
     * @param {*} tags
     * @memberof NewPostForm
     */
    retrieveTags = (tags) => {
        this.setState({
            postTags: tags
        })
    }

    

    render(){

        const listCategories = this.state.categories.map((categorie,i) => {
            return(<option key={i} value={categorie.id} >{categorie.title}</option>);
        })
        return(
            <Fragment>

                <Form onSubmit={this.handleSubmit} onKeyPress={event => {if (event.key === 'Enter' && event.target.localName !== "textarea") {event.preventDefault();}}} noValidate>
                    <Form.Group controlId="newPost.titre">
                        <Form.Label>Titre {this.state.errors.postTitle.length > 0 && <span className='error'>{this.state.errors.postTitle}</span>}</Form.Label>
                        <Form.Control value={this.state.postTitle} name="title" type="text" placeholder="Titre du post" onChange={this.handleChange}/>
                    </Form.Group>
                    <Form.Group controlId="newPost.category">
                        <Form.Label>Catégorie  {this.state.errors.postCategory.length > 0 && <span className='error'>{this.state.errors.postCategory}</span>}</Form.Label>
                        <Form.Control name="category" as="select" onChange={this.handleChange}>
                            <option value="">Choisir une catégorie</option>
                            {listCategories}
                        </Form.Control>
                    </Form.Group>

                    <Form.Group controlId="newPost.content">
                        <Form.Label>Contenu {this.state.errors.postContent.length > 0 && <span className='error'>{this.state.errors.postContent}</span>}</Form.Label>
                        <Form.Control name="content" as="textarea" rows={5} onChange={this.handleChange}/>
                    </Form.Group>
                    <Form.Group controlId="newPost.tags">
                        <Form.Label>Tags (Appuyer sur entrée pour valider)</Form.Label>
                        <Tags name="postTags" tagsList={this.retrieveTags} tags={this.state.postTags} suggestions={this.state.tags}/>
                    </Form.Group>
                    <Row className="justify-content-center">
                        {
                        (!this.state.errors.postTitle && !this.state.errors.postCategory && !this.state.errors.postContent ) ? 
                            <Button type="submit">Envoyer!</Button>
                        :
                            <Button type="submit" disabled>Envoyer!</Button>
                        }
                    </Row>
                </Form>
                {this.state.redirect && (<Redirect to={this.state.redirectLink}/>)}
            </Fragment>
        )
    }
}
export default NewPostForm;