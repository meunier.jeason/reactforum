import React, {Component, Fragment} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { API } from '../../constants';
import Tags from './tags';
import LoadingSpinner from '../utils/loadingSpinner';

/**
 * Composant du formulaire de mise a jour des posts
 * Enfant de UpdateModal
 * Props requis: updateList (fonction parente permettant le refresh), 
 * modalUpdate (fonction parente permettant le toggle de la modal),
 * postInfos (objet contenant les infos de l a modifier)
 *
 * @class UpdatePost
 * @extends {Component}
 */
class UpdatePost extends Component {
    
    /**
     * Creates an instance of UpdatePost.
     * @param {updateList, modalUpdate, postInfos} props
     * @memberof UpdatePost
     */
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            categoriesLoad: false,
            tagsLoad: false,
            existingTagsLoad: false,
            stateLoad: false,
            categories: null,
            states: null,
            tags: [],
            id: this.props.postInfos.id,
            title: this.props.postInfos.title,
            category: this.props.postInfos.category.id,
            content: this.props.postInfos.content,
            postTags: this.props.postInfos.tags,
            state: this.props.postInfos.state.id,
            errors: {
                postTitle: '',
                postCategory: '',
                postContent: ''
            }
        };
    }

    /**
     * Fonction permettant un affichage du loader avec durée choisi
     *
     * @return {Promise} 
     * @memberof UpdatePost
     */
    loadingTime(){
        return new Promise(resolve => setTimeout(resolve, 300))
    }
    

    /**
     * Au didMount du composant, recupere les tags et les catégories du forum
     *
     * @memberof UpdatePost
     */
    componentDidMount() {
        this.loadingTime().then(() => {
            !this.state.existingTagsLoad && this.getExistingTags();
            !this.state.categoriesLoad && this.getCategories();
            !this.state.tagsLoad && this.getTags();
            !this.state.stateLoad && this.getStates();
        })

    }

    /**
     * Fonction qui handle le submit du formulaire
     * Si formulaire valide, alors envoi la réponse modifié à l'api
     *
     * @param {event} event
     * @memberof UpdatePost
     */
    handleSubmit = (event) => {
        event.preventDefault();
        if(this.validateForm(this.state.errors)) {
            console.info('Valid Form')
            let {id, title, content, postTags, category, state} = this.state
            let tagsObject = postTags.map(elem => {
                return({"title":elem})
            })
            let data = {
                title : title,
                content: content,
                tags: tagsObject,
                state: `/api/state/${state}`,
                category: `/api/category/${category}`
            }
            const requestOptions = {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                credentials: 'include',
                body: JSON.stringify(data)
            };
            fetch(`${API}/post/${id}`, requestOptions)
            .then(res => res.json())
                .then(res => {
                    this.props.updateList();
                    this.props.modalUpdate();
                })
                .catch(console.log)

        }else{
            console.error('Invalid Form')
            // this.props.showToast()
        }
    }
    //Permet de verifier si il ya des erreurs (au cas ou le "disabled" du bouton est supprimé dans la console)
    /**
     * Fonction de validation du formulaire, appelé par handleSubmit()
     * Prend l'objet contenant les erreurs en parametre (venant du state)
     *
     * @param {Object} errors
     * @return {boolean}
     * @memberof UpdatePost
     */
    validateForm = (errors) => {
        let valid = true;
        Object.values(errors).forEach(
          // Si les errors ne sont pas vide, passe le valid à false
          (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    //Permet de verifier en temps réel si les champs sont correctements remplis
    /**
     * Fonction qui controle les champs du formulaire
     * Affiche les erreurs en temps réel et les met dans le state
     *
     * @param {event} event
     * @memberof UpdatePost
     */
    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;
      
        switch (name) {
          case 'title': 
            errors.postTitle = 
              value.length < 6
                ? 'Le taille du titre doit etre supérieure à 5 charactères!'
                : '';
            break;
            case 'category': 
            errors.postCategory = 
              value === ''
                ? 'Merci de selectionner une catégorie'
                : '';
            break;
            case 'content': 
            errors.postContent = 
            value.length < 31
                ? 'Le taille du contenu doit etre supérieure à 30 charactères!'
                : '';
            break;
          default:
            break;
        }
      
        this.setState({errors, [name]: value})
    }

    /**
     * Fonction qui set les nouveaux tags du post
     *
     * @param {*} tags
     * @memberof UpdatePost
     */
    retrieveTags = (tags) => {
        this.setState({
            postTags: tags
        })
    }

    /**
     * Récupere les tags du post (props) et en fait un array
     *
     * @memberof UpdatePost
     */
    getExistingTags(){
        let tagsList = [];
        this.props.postInfos.tags.forEach(element => {
            tagsList.push(element.title)
          });
          this.setState({
              postTags: tagsList,
              existingTagsLoad: true
          });
          (this.state.categoriesLoad && this.state.tagsLoad && this.state.existingTagsLoad && this.state.statesLoad) && this.setState({loading: false});
    }

    /**
     * Récupere les catégorie du forum depuis l'api
     *
     * @memberof UpdatePost
     */
    getCategories(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        };
        fetch(`${API}/categories`, requestOptions)
            .then(res => res.json())
            .then((data) => {
                this.setState({ 
                    categories: data['hydra:member'],
                    categoriesLoad: true,
                });
                (this.state.categoriesLoad && this.state.tagsLoad && this.state.existingTagsLoad && this.state.statesLoad) && this.setState({loading: false});

            })
            .catch(console.log)
    }

    /**
     * Récupere les Etats de post depuis l'api
     *
     * @memberof UpdatePost
     */
    getStates(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        };
        fetch(`${API}/states`, requestOptions)
            .then(res => res.json())
            .then((data) => {
                this.setState({ 
                    states: data['hydra:member'],
                    statesLoad: true,
                });
                
                (this.state.categoriesLoad && this.state.tagsLoad && this.state.existingTagsLoad && this.state.statesLoad) && this.setState({loading: false});

            })
            .catch(console.log)
    }

    /**
     * Récupere les tags existants depuis l'api
     *
     * @memberof UpdatePost
     */
    getTags(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        };
        fetch(`${API}/tags`, requestOptions)
            .then(res => res.json())
            .then((data) => {
                this.setState({ 
                    tags: data['hydra:member'],
                    tagsLoad: true,
                });
                (this.state.categoriesLoad && this.state.tagsLoad && this.state.existingTagsLoad && this.state.statesLoad) && this.setState({loading: false});
            })
            .catch(console.log)
    }    

    render(){
        let listCategories;
        let listState;
        if(!this.state.loading){
            listCategories = this.state.categories.map((categorie,i) => {
                    return(<option key={i} value={categorie.id} >{categorie.title}</option>);
            })
            listState = this.state.states.map((state,i) => {
                return(<option key={i} value={state.id} >{state.title}</option>);
            })
        }

        return(
            this.state.loading ?
                <LoadingSpinner/>
            :
                <Fragment>

                    <Form onSubmit={this.handleSubmit} onKeyPress={event => {if (event.key === 'Enter' && event.target.localName !== "textarea") {event.preventDefault();}}} noValidate>
                        <Form.Group controlId="updatePost.titre">
                            <Form.Label>Titre {this.state.errors.postTitle.length > 0 && <span className='error'>{this.state.errors.postTitle}</span>}</Form.Label>
                            <Form.Control value={this.state.title} name="title" type="text" placeholder="Titre du post" onChange={this.handleChange}/>
                        </Form.Group>
                        <Form.Group controlId="updatePost.category">
                            <Form.Label>Catégorie  {this.state.errors.postCategory.length > 0 && <span className='error'>{this.state.errors.postCategory}</span>}</Form.Label>
                            <Form.Control name="category" as="select" value={this.state.category} onChange={this.handleChange}>
                                <option value="">Choisir une catégorie</option>
                                {listCategories}
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="updatePost.content">
                            <Form.Label>Contenu {this.state.errors.postContent.length > 0 && <span className='error'>{this.state.errors.postContent}</span>}</Form.Label>
                            <Form.Control name="content" as="textarea" value={this.state.content} rows={5} onChange={this.handleChange}/>
                        </Form.Group>
                        <Form.Group controlId="updatePost.tags">
                            <Form.Label>Tags (Appuyer sur entrée pour valider)</Form.Label>
                            <Tags name="postTags" tagsList={this.retrieveTags} tags={this.state.postTags} suggestions={this.state.tags}/>
                        </Form.Group>
                        <Form.Group controlId="updatePost.state">
                            <Form.Label>État du Post</Form.Label>
                            <Form.Control name="state" as="select" value={this.state.state} onChange={this.handleChange}>
                                {listState}
                            </Form.Control>
                        </Form.Group>                        
                        {
                        (!this.state.errors.postTitle && !this.state.errors.postCategory && !this.state.errors.postContent ) ? 
                            <Button type="submit">Envoyer!</Button>
                        :
                            <Button type="submit" disabled>Envoyer!</Button>
                        }
                    </Form>
                    {/* {this.state.redirect && (<Redirect to={this.state.redirectLink}/>)} */}
                </Fragment>
        )
    }
}
export default UpdatePost;