import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row'
import Card from 'react-bootstrap/Card'
import { Redirect } from 'react-router'
import { API } from '../../constants';


/**
 * Composant contenant le formulaire de réponse à un post
 * Enfant de PostDetails
 * Props requis: postSlug (string contenant le slug d'un post), postId(int correspondant a l'id du post)
 *
 * @class NewResponseForm
 * @extends {Component}
 */
class NewResponseForm extends Component {
    
    /**
     * Creates an instance of NewResponseForm.
     * @param {postSlug, postId} props
     * @memberof NewResponseForm
     */
    constructor(props) {
        // const [state, setstate] = useState(false)
        super(props);
        this.state = {
            redirect: false,
            redirectLink: null,
            content: null,
            errors: {
                postContent: 'Merci de remplir le contenu de votre réponse'
            }
        };
    }

    /**
     * Fonction qui handle le submit du formulaire
     * Si formulaire valide, alors envoi la réponse à l'api
     *
     * @param {*} event
     * @memberof NewResponseForm
     */
    handleSubmit = (event) => {
        event.preventDefault();
        if(this.validateForm(this.state.errors)) {
            console.info('Valid Form')
            let {content} = this.state
            let {postId, postSlug} = this.props
            let data = {
                content: content,
                post: '/api/post/'+postId
            }
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                credentials: 'include',
                body: JSON.stringify(data)
            };
            fetch(`${API}/response`, requestOptions)
            .then(res => res.json())
                .then(res => {
                    this.setState({
                        redirect: true,
                        redirectLink: `/post/${postId}/${postSlug}`
                    })
                })
                .catch(console.log)
        }else{
            console.error('Invalid Form')
        }
    }
    //Permet de verifier si il ya des erreurs (au cas ou le "disabled" du bouton est supprimé dans la console)
    /**
     * Fonction de validation du formulaire, appelé par handleSubmit()
     * Prend l'objet contenant les erreurs en parametre (venant du state)
     * 
     * @param {Object} errors
     * @return {boolean}
     * @memberof NewResponseForm
     */
    validateForm = (errors) => {
        let valid = true;
        Object.values(errors).forEach(
          // Si les errors ne sont pas vide, passe le valid à false
          (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    //Permet de verifier en temps réel si les champs sont correctements remplis
    /**
     * Fonction qui controle les champs du formulaire
     * Affiche les erreurs en temps réel et les met dans le state
     *
     * @param {event} event
     * @memberof NewResponseForm
     */
    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;
        errors.postContent = 
        value.length < 31
            ? 'Le taille du contenu doit etre supérieure à 30 charactères!'
            : '';
        this.setState({errors, [name]: value})
    }
    

    render(){

        return(
            <Card>
                <Card.Header>Répondre:</Card.Header>
                <Card.Body style={{backgroundColor:'#e9ecef'}}>
                    <Form onSubmit={this.handleSubmit} onKeyPress={event => {if (event.key === 'Enter' && event.target.localName !== "textarea") {event.preventDefault();}}} noValidate>
                        <Form.Group controlId="newResponse.content">
                            <Form.Label>Contenu de votre réponse {this.state.errors.postContent.length > 0 && <span className='error'>{this.state.errors.postContent}</span>}</Form.Label>
                            <Form.Control name="content" as="textarea" rows={5} onChange={this.handleChange}/>
                        </Form.Group>
                        <Row className="justify-content-center">
                            {
                            !this.state.errors.postContent ?
                                
                                <Button type="submit" className="justify-content-center">Repondre!</Button>
                            :
                                <Button type="submit" className="justify-content-center" disabled>Repondre!</Button>
                            }
                        </Row>
                    </Form>
                </Card.Body>
                
                {this.state.redirect && (<Redirect to={this.state.redirectLink}/>)}
            </Card>
        )
    }
}
export default NewResponseForm;