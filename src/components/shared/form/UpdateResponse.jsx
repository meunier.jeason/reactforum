import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import { API } from '../../constants';

/**
 * Composant du formulaire de mise a jour des réponses
 * Enfant de UpdateModal
 * Props requis: updateList (fonction parente permettant le refresh), 
 * modalUpdate (fonction parente permettant le toggle de la modal),
 * responseInfos (objet contenant les infos de la réponse a modifier)
 *
 * @class UpdateResponse
 * @extends {React.Component}
 */
class UpdateResponse extends React.Component {
    /**
     * Creates an instance of UpdateResponse.
     * @param {updateList, modalUpdate, responseInfos} props
     * @memberof UpdateResponse
     */
    constructor(props){
        super(props)
        this.state={
            content: this.props.responseInfos.content,
            id: this.props.responseInfos.id,
            errors: {
                postContent: ''
            }
        }
    }

    /**
     * Fonction qui handle le submit du formulaire
     * Si formulaire valide, alors envoi la réponse modifié à l'api
     *
     * @param {event} event
     * @memberof UpdateResponse
     */
    handleSubmit = (event) => {
        event.preventDefault();
        if(this.validateForm(this.state.errors)) {
            console.info('Valid Form')
            let {content, id} = this.state
            let data = {
                content: content
            }
            const requestOptions = {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                credentials: 'include',
                body: JSON.stringify(data)
            };
            fetch(`${API}/response/${id}`, requestOptions)
            .then(res => res.json())
                .then(res => {
                    this.props.updateList();
                    this.props.modalUpdate();
                })
                .catch(console.log)
        }else{
            console.error('Invalid Form')
        }
    }

    //Permet de verifier si il ya des erreurs (au cas ou le "disabled" du bouton est supprimé dans la console)
    /**
     * Fonction de validation du formulaire, appelé par handleSubmit()
     * Prend l'objet contenant les erreurs en parametre (venant du state)
     *
     * @param {Object} errors
     * @return {boolean}
     * @memberof UpdateResponse
     */
    validateForm = (errors) => {
        let valid = true;
        Object.values(errors).forEach(
          // Si les errors ne sont pas vide, passe le valid à false
          (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    //Permet de verifier en temps réel si les champs sont correctements remplis
    /**
     * Fonction qui controle les champs du formulaire
     * Affiche les erreurs en temps réel et les met dans le state
     *
     * @param {event} event
     * @memberof UpdateResponse
     */
    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;
        errors.postContent = 
        value.length < 31
            ? 'Le taille du contenu doit etre supérieure à 30 charactères!'
            : '';
        this.setState({errors, [name]: value})
    }
        
    render(){
        return(
            <Form onSubmit={this.handleSubmit} onKeyPress={event => {if (event.key === 'Enter' && event.target.localName !== "textarea") {event.preventDefault();}}} noValidate>
                <Form.Group controlId="newResponse.content">
                    <Form.Label>{this.state.errors.postContent.length > 0 && <span className='error'>{this.state.errors.postContent}</span>}</Form.Label>
                    <Form.Control name="content" as="textarea" rows={3} onChange={this.handleChange} defaultValue={this.props.responseInfos.content}/>
                </Form.Group>
                {
                !this.state.errors.postContent ? 
                    <Button type="submit">Modifier!</Button>
                :
                    <Button type="submit" disabled>Modifier!</Button>
                }
            </Form>
        )
    }
}
export default UpdateResponse;