import React, {Fragment} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import { API } from '../../constants';

/**
 * Composant affichant la modal d'ajout d'une categorie
 * Enfant de AdminCategories
 * Props requis: show (boolean parent d'etat de modal),
 * modalUpdate (fonction parente d'affichage de la modal),
 * updateList(fonction parente de refresh de données)
 * @class CategoryModal
 * @extends {React.Component}
 */
class CategoryModal extends React.Component {
    /**
     * Creates an instance of CategoryModal.
     * @param {show, modalUpdate, updateList} props
     * @memberof CategoryModal
     */
    constructor(props){
        super(props)
        this.state={
            modalToggle: props.show,
            title: null,
            errors: {
                categoryTitle: 'Merci de remplir le nom de la catégorie'
            }
        }
    }

    // Submit du post
    /**
     * Fonction qui handle le submit du formulaire
     * Si formulaire valide, alors envoi la réponse modifié à l'api
     *
     * @param {*} event
     * @memberof CategoryModal
     */
    handleSubmit = (event) => {
        event.preventDefault();
        if(this.validateForm(this.state.errors)) {
            console.info('Valid Form')
            let {title} = this.state
            let data = {
                title: title,
                icon: '#000000'
            }
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                credentials: 'include',
                body: JSON.stringify(data)
            };
            fetch(`${API}/category`, requestOptions)
            .then(res => res.json())
                .then(res => {
                    this.props.modalUpdate()
                    this.props.updateList();
                })
                .catch(console.log)
        }else{
            console.error('Invalid Form')
        }
    }

    //Permet de verifier si il ya des erreurs (au cas ou le "disabled" du bouton est supprimé dans la console)
    /**
     * Fonction de validation du formulaire, appelé par handleSubmit()
     * Prend l'objet contenant les erreurs en parametre (venant du state)
     *
     * @param {Object} errors
     * @memberof CategoryModal
     */
    validateForm = (errors) => {
        let valid = true;
        Object.values(errors).forEach(
          // Si les errors ne sont pas vide, passe le valid à false
          (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    //Permet de verifier en temps réel si les champs sont correctements remplis
    /**
     * Fonction qui controle les champs du formulaire
     * Affiche les erreurs en temps réel et les met dans le state
     *
     * @param {event} event
     * @memberof CategoryModal
     */
    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;
        errors.categoryTitle = 
        value.length < 1
            ? 'Le nom de la catégorie doit etre supérieure à 1 caractère!'
            : '';
        this.setState({errors, [name]: value})
    }


    render(){
        if(this.props.show){

            return(
                <div className="modaltest">
                    <div className='modal-content'>
                        <span className="close text-right" onClick={() =>this.props.modalUpdate()}>&times;</span>
                        <h5>Ajouter une catégorie:</h5>
                        <Form onSubmit={this.handleSubmit} onKeyPress={event => {if (event.key === 'Enter') {event.preventDefault();}}} noValidate>
                            <Form.Group controlId="newResponse.content">
                                <Form.Label>Nom de la catégorie {this.state.errors.categoryTitle.length > 0 && <span className='error'>{this.state.errors.categoryTitle}</span>}</Form.Label>
                                <Form.Control name="title" type="text" placeholder="Linux, Windows..." onChange={this.handleChange}/>
                            </Form.Group>
                            {
                            !this.state.errors.categoryTitle ? 
                                <Button type="submit">Repondre!</Button>
                            :
                                <Button type="submit" disabled>Repondre!</Button>
                            }
                        </Form>                      
                    </div>
                </div>
            )
        } else{
            return <Fragment/>
        }
    }
}
export default CategoryModal;
