import React, { Component } from 'react';
import { API } from '../../constants.js';
import PostLine from './postLine';
import Pagination from '../utils/pagination';

import Table from 'react-bootstrap/Table';
import { Fragment } from 'react';

/**
 * Composant affichant le tableau des posts sur la page Admin
 * Enfant de UserInfos, Parent de PostLine, Pagination
 * Props requis: aucun
 *
 * @class AdminPost
 * @extends {Component}
 */
class AdminPost extends Component {
    
    state = {
        posts: [],
    }

    /**
     * Fonction qui fetch tous les posts de l'api
     *
     * @param {number} [newPage=1]
     * @memberof AdminPost
     */
    getAllPosts(newPage = 1){
        const page = newPage;
        const filters = "?order[createdAt]=desc&page="+page;
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/posts`+filters, requestOptions)
        .then(res => res.json())
          .then(res => {
            let nbPages;
            if(res['hydra:view'].hasOwnProperty(['hydra:last'])){
              const iriLastPages = res['hydra:view']['hydra:last'];
              const chars = iriLastPages.split('&page=');
              nbPages = chars[1]
            } else{
                if(this.state.currentPage !== 1){
                  this.getAllPosts(1)
                }else{
                  nbPages = "1";
                }
            }
            this.setState({ 
                posts: res['hydra:member'],
                nbPages: nbPages,
                currentPage: page,
            })
        })
    }

    /**
     * Au didMount du composant, fetch l'api
     *
     * @memberof AdminPost
     */
    componentDidMount(){
        this.getAllPosts();
    }

    /**
     * Fonction de refresh en cas d'update/delete d'un post
     *
     * @param {*} newPage Page actuelle
     * @memberof AdminPost
     */
    pageChange(newPage){
        this.getAllPosts(newPage);
        
    }

    render(){
        return (
            <Fragment >
                <Table className="bg-light" striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>Etat</th>
                            <th>Titre</th>
                            <th>Propriétaire</th>
                            <th className="d-none d-md-table-cell">Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.posts.map((post,i , arr) => {
                            return(
                                <PostLine key={post.id} postInfos={post} updateList={() => this.pageChange(this.state.currentPage)} /> 
                            )                                   
                        })}
                    </tbody>
                </Table>
                {this.state.nbPages > 1 && <Pagination maxPage={this.state.nbPages} currentPage={this.state.currentPage} pageChange={this.pageChange.bind(this)}/>}
            </Fragment>
        );
    }
}
export default AdminPost;