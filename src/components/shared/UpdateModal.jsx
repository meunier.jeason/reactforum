// import FormComponent from 'FormComponent'
import React, {Fragment} from 'react';
import UpdateResponse from './form/UpdateResponse';
import UpdatePost from './form/UpdatePost';

/**
 * Composant affichant la Modal de modification de post/réponse
 *
 * @class UpdateModal
 * @extends {React.Component}
 */
class UpdateModal extends React.Component {
    /**
     * Creates an instance of UpdateModal.
     * Props requis: show(etat de la modal), modalUpdate(changement d'etat affichage modal),
     * updateList(fonction parente qui refresh les données affichées), type(string "post" ou "response"),
     * postInfos (Objet contenant les infos du post ou de la réponse a modifier)
     * @param {show, modalUpdate, updateList, type, postInfos} props
     * @memberof UpdateModal
     */
    constructor(props){
        super(props)
        this.state={
            modalToggle: props.show,
        }
    }


    render(){
        if(this.props.show){
            if(this.props.type === "response"){
            return(
                <div className="modaltest">
                    <div className='modal-content'>
                        <span className="close text-right" onClick={() =>this.props.modalUpdate()}>&times;</span>
                        <h5>Modifier votre réponse:</h5>
                        <UpdateResponse updateList={this.props.updateList} modalUpdate={this.props.modalUpdate} responseInfos={this.props.responseInfos}/>                        
                    </div>
                </div>
            )
            }else if(this.props.type === "post"){
                return(
                    <div className="modaltest">
                        <div className='modal-content'>
                            <span className="close text-right" onClick={() =>this.props.modalUpdate()}>&times;</span>
                            <h5>Modifier votre Post:</h5>
                            <UpdatePost updateList={this.props.updateList} modalUpdate={this.props.modalUpdate} postInfos={this.props.postInfos}/>                        
                        </div>
                    </div>
                )
            }
        } else {
            return(
                <Fragment/>
            )
        }
    }
}
export default UpdateModal;