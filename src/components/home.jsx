import React, {Component} from 'react';
import PostTableRecent from './shared/post/postTableRecent';

/**
 * Composant principal de la page d'accueil
 * Parent du composant PostTableRecent qui
 * affiche les posts du forum dans l'ordre décroissant
 *
 * @class Home
 * @extends {Component}
 */
class Home extends Component {
    render(){
        return (
            <PostTableRecent userConnect={this.props.userInfos} isAuth={this.props.isAuth}></PostTableRecent>
        );
    }
}
export default Home;