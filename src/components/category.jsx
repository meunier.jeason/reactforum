import React, {Component} from 'react';
import PostTableCategory from './shared/post/postTableCategory';

/**
 * Composant principal de la page catégorie
 * Parent du composant PostTableCategory qui
 * affiche les posts du forum selon une catégorie
 *
 * @class Home
 * @extends {Component}
 */
class Category extends Component {
    
    render(){
        return (
            <PostTableCategory id={this.props.match.params.id} userConnect={this.props.userInfos} isAuth={this.props.isAuth}></PostTableCategory>
        );
    }
}
export default Category;