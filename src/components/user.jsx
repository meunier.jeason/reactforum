import React, {Component} from 'react';
import UserInfos from './shared/user/userInfos'

/**
 * Composant principal de la page utilisateur
 * Parent du composant UserInfos qui
 * affiche les posts et les réponses d'un utilsateur.
 * Si l'utilsateur connecté est admin, il aura accès depuis ici à la page d'admin du forum
 *
 * @class User
 * @extends {Component}
 */
class User extends Component {
    render(){
        return (
            <UserInfos isAuth={this.props.isAuth} reload={this.props.reload} userConnect={this.props.userInfos} id={this.props.match.params.id} username={this.props.match.params.username}/>
        );
    }
}
export default User;