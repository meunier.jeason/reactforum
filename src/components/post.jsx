import React, {Component} from 'react';
import PostDetails from './shared/post/postDetails';

/**
 * Composant principal de la page detaillé d'un post
 * Parent du composant PostDetails qui
 * affiche les détails d'un post, le formulaire de réponse et les réponses
 *
 * @class Post
 * @extends {Component}
 */
class Post extends Component {
    render(){
        return (
            <PostDetails isAuth={this.props.isAuth} userInfos={this.props.userInfos} slug={this.props.match.params.slug} id={this.props.match.params.id}></PostDetails>
        );
    }
}
export default Post;