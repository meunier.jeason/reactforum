import React, {Component} from 'react';
import PostSearch from './shared/post/postSearch';

/**
 * Composant principal de la page de résultat de recherche
 * Parent du composant PostSearch qui affiche les posts 
 * contenant la critere dans le titre ou le contenu  
 *
 * @class Search
 * @extends {Component}
 */
class Search extends Component {
    render(){
        return (
            <PostSearch criteria={this.props.match.params.criteria} userConnect={this.props.userInfos} isAuth={this.props.isAuth}></PostSearch>
        );
    }
}
export default Search;