import React, { Component, Fragment } from 'react';
import { API } from './constants.js';
import NewPostForm from './shared/form/newPostForm';
import LoadingSpinner from './shared/utils/loadingSpinner';
import Toast from 'react-bootstrap/Toast';

import Jumbotron from 'react-bootstrap/Jumbotron';


/**
 * Composant contenant le formulaire de creation d'un post
 * Enfant de App
 * Props requis: isAuth (boolean user connecté ou non)
 *
 * @class NewPost
 * @extends {Component}
 */
class NewPost extends Component {

    /**
     * Creates an instance of NewPost.
     * @param {isAuth} props
     * @memberof NewPost
     */
    constructor(props) {
        super(props);
        this.state = {
          loading: true,
          show: false,
          isAuth: props.isAuth,
          categories: [],
          categoriesLoad: false,
          tags: [],
          tagsLoad: false,
        };
    }

    /**
     * Fonction permettant un affichage du loader avec durée choisi
     *
     * @return {Promise} 
     * @memberof NewPost
     */
    loadingTime(){
        return new Promise(resolve => setTimeout(resolve, 300))
    }
    
    /**
     * Au lancement du composant, affiche le Loading Spinner
     * puis si un user est connecté, récupere les catégories 
     * et les tags du forum depuis l'api
     *
     * @memberof NewPost
     */
    componentDidMount() {
        this.loadingTime().then(() => {
            if(this.state.isAuth === true){
                !this.state.categoriesLoad && this.getCategories();
                !this.state.tagsLoad && this.getTags();
                
            }
        })

    }
    
    /**
     * Affichage du Toast
     *
     * @memberof NewPost
     */
    showToast = () => {
        this.setState({
            show: true
        })
    }

    /**
     * * Appel à l'api pour récuperer les catégories du forum
     *
     * @memberof NewPost
     */
    getCategories(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/categories`, requestOptions)
            .then(res => res.json())
            .then((data) => {
                this.setState({ 
                    categories: data['hydra:member'],
                    categoriesLoad: true,
                });
                (this.state.categoriesLoad && this.state.tagsLoad) && this.setState({loading: false});

            })
            .catch(console.log)
    }

    /**
     * * Appel à l'api pour récuperer les tags du forum
     *
     * @memberof NewPost
     */
    getTags(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        };
        fetch(`${API}/tags`, requestOptions)
            .then(res => res.json())
            .then((data) => {
                this.setState({ 
                    tags: data['hydra:member'],
                    tagsLoad: true,
                });
                (this.state.categoriesLoad && this.state.tagsLoad) && this.setState({loading: false});
            })
            .catch(console.log)
    }
    
    render(){
        return (
            <Fragment>
                {!this.state.isAuth ? 
                    <div className="noPage">
                        <div className="cont_principal cont_error_active">
                            <div className="cont_error">
                                <h3>Vous n'êtes pas connecté</h3>
                                <p>Impossible de poster un article</p>
                            </div>
                            <div className="cont_aura_1"></div>
                            <div className="cont_aura_2"></div>
                        </div>
                    </div>

                 : this.state.loading ?
                    <LoadingSpinner/>
                 : 
                    <Fragment>
                        <Toast style={{position:'absolute',top:'70px',right:'50px',backgroundColor:'#eb5a46',color:'white'}} onClose={() => this.setState({show: false})} show={this.state.show} animation={false} delay={3000} autohide>
                            <Toast.Header>
                                <strong className="mr-auto">Erreur</strong>
                            </Toast.Header>
                            <Toast.Body>Impossible de valider le formulaire!</Toast.Body>
                        </Toast>
                        <div className="container mt-4">
                            <Jumbotron className="pt-4">
                                <h1 className="text-center">Ecrire un nouveau Post</h1>
                                <hr/>
                                <NewPostForm showToast={this.showToast} categories={this.state.categories} tags={this.state.tags}/>
                            </Jumbotron>
                        </div>
                    </Fragment>
                }

            </Fragment>


        );
    }
}
export default NewPost;